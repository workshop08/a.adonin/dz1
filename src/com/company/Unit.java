package com.company;

import java.util.logging.Logger;

public abstract class Unit implements Actions {
    protected int health, damage, attackRange, id, lastAttack;
    AttackType attackType;
    UnitType unitType;
    static int idcount = -1;
    protected long attackSpeed;
    protected String name;


    public Unit(String name, int health, int damage, int attackRange, long attackSpeed) {
        this.name = name;
        this.health = health;
        this.damage = damage;
        this.attackRange = attackRange;
        this.attackSpeed = attackSpeed;
        this.id = idCounter();
    }

    int idCounter() {
        idcount++;
        return idcount;
    }

    int getId() {
        return id;
    }

    void printStat() {
        System.out.println("Тип юнита:" + unitType);
        System.out.println("Количество здоровья:" + health);
        System.out.println("Урон:" + damage);
        System.out.println("Тип атаки:" + attackType);
        System.out.println("Дальность атаки:" + attackRange);
    }

    public void getDamage(int damageTaken) {
        health -= damageTaken;
    }

    public void fight(Unit enemy) {
        Logger logger = Logger.getLogger(Unit.class.getName());
        FightThread fight = new FightThread(this, enemy);
        FightThread fight2 = new FightThread(enemy, this);
        fight.start();
        fight2.start();
        while (!fight.isInterrupted() && !fight2.isInterrupted()) {
            if (fight.isInterrupted() || fight2.isInterrupted()) {
                if ((this.health <= 0) && (enemy.health <= 0)) {
                    logger.info("Draw");
                } else if (this.health <= 0) {
                    logger.info(enemy.name + " has won.");
                } else {
                    logger.info(this.name + " has won.");
                }
            }
        }
    }

}
