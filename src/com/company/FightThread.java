package com.company;

import java.io.IOException;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;


public class FightThread extends Thread {
    public String FIGHT_LISTENER;
    Unit unit1, unit2;
    private static final String template = "fight";
    private SwingPropertyChangeSupport pcSupport = new SwingPropertyChangeSupport(this);
    static int idcount = -1;
    protected int id;

    public FightThread(Unit unit1, Unit unit2) {
        this.unit1 = unit1;
        this.unit2 = unit2;
    }

    public void run(){
        Logger logger = Logger.getLogger(FightThread.class.getName());
        FileHandler fh;
        String FIGHT_LISTENER = template + getFightId();
        idCounter();
        try {
            fh = new FileHandler("log.txt");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        Random random = new Random();
        while (!this.isInterrupted()){
            while ((unit2.health >= 0) && (unit1.health >= 0)) {
                synchronized (this) {
                    int oldValue = unit2.health;
                    int rate = random.nextInt(3);
                    logger.info(unit1.name + " атакует");
                    logger.info("Множитель урона при атаке: " + rate);
                    unit2.getDamage(rate * unit1.damage);
                    int newValue = unit2.health - rate * unit1.damage;
                    logger.info("У "+unit2.name+" осталось здоровья: " + unit2.health);
                    pcSupport.firePropertyChange(FIGHT_LISTENER, oldValue, newValue);
                    try {
                        wait(unit1.attackSpeed);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
            this.interrupt();
        }
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcSupport.addPropertyChangeListener(name, listener);
    }

    int idCounter() {
        idcount++;
        return idcount;
    }

    int getFightId() {
        return id;
    }

    String getOne() {
        return "1";
    }

    String getTwo() {
        return "2";
    }

}
