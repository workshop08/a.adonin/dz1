package com.company;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;


public class Main extends Application {

    public void start(Stage primaryStage){
        Logger logger = Logger.getLogger(Main.class.getName());
        logger.setLevel(Level.INFO);
        FileHandler fh;

        try {
            fh = new FileHandler("log.txt");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        Knight knight = new Knight( "Knight",500,35, 3,3000);
        knight.printStat();
        System.out.println("id:"+ knight.getId());
        System.out.println();

        Archer archer = new Archer("Archer",500,30, 7,2000);

        knight.fight(archer);
        archer.fight(knight);

        logger.info("Программа закончена");

        FightGraphics.start(primaryStage);
    }

    public static void main(String[] args) {
        launch();

    }
}
