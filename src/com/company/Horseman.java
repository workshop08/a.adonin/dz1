package com.company;

public class Horseman extends Unit{

    public Horseman(String name, int health, int damage, int attack_range, long attackSpeed){
        super(name, health, damage, attack_range, attackSpeed);
        attackType = AttackType.SEMIRANGE;
        unitType = UnitType.HORSEMAN;
    }

    public void attack(){
        System.out.println("*Кавалерист атакует*");
    }

    public void move(){
        System.out.println("*Кавалерист скачет*");
    }
}
