package com.company;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class FightGraphics {
    private static final String FINAL_LINE = "fight" ;
    Unit unit1, unit2;
    FightThread fightThread;
    public FightGraphics(Unit unit1, Unit unit2) {
        this.unit1 = unit1;
        this.unit2 = unit2;
    }

    public static void start(Stage primaryStage){
        String defaultLine = "Имя юнита: " + unit1.name;
        Group root = new Group();
        Scene scene = new Scene(root, 1200, 1200);
        primaryStage.setTitle("Fight");
        primaryStage.setScene(scene);

        Text headerLeft = new Text(100,100,defaultLine);
        headerLeft.setFont(Font.font(20));
        Text headerRight = new Text(100,1000,defaultLine);
        headerRight.setFont(Font.font(20));
        Text statLeft = new Text(200,100,Integer.toString(fightThread.unit1.health));
        statLeft.setFont(Font.font(20));
        Text statRight = new Text(200,1000,Integer.toString(fightThread.unit2.health));
        statLeft.setFont(Font.font(20));
        fightThread.addPropertyChangeListener("fight1", pcEvent -> updateInfo(statLeft, unit1));
        fightThread.addPropertyChangeListener("fight2", pcEvent -> updateInfo(statRight, unit2));

        root.getChildren().add(headerLeft);
        root.getChildren().add(headerRight);
        root.getChildren().add(statLeft);
        root.getChildren().add(statRight);
        primaryStage.show();

    }

    private void updateInfo(Text text, Unit unit)
    {
        text.setText("Количесвто здоровья: " + unit.health);
    }


}