package com.company;

public class Archer extends Unit{

    public Archer(String name, int health, int damage, int attack_range, long attackSpeed){
        super(name, health, damage, attack_range, attackSpeed);
        attackType = AttackType.RANGE;
        unitType = UnitType.ARCHER;
    }

    public void attack(){
        System.out.println("*Лучник выстрелил*");
    }

    public void move(){
        System.out.println("*Лучник идет*");
    }

}
