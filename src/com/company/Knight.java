package com.company;

public class Knight extends Unit{

    public Knight(String name, int health, int damage, int attack_range, long attackSpeed){
        super(name, health, damage, attack_range, attackSpeed);
        attackType = AttackType.MELEE;
        unitType = UnitType.KNIGHT;
    }

    public void attack(){
        System.out.println("*Мечник атакует*");
    }
    public void move(){
        System.out.println("*Мечник бежит*");
    }
}
